//
//  CommonUtils.m
//  20141020_iOS8_NotificationAppSample
//
//  Created by aCetylAcid on 2014/10/21.
//  Copyright (c) 2014年 aCetylAcid. All rights reserved.
//

#import "CommonUtils.h"

@implementation CommonUtils

+ (BOOL)isIOS7orEarlier
{
    NSString *currentVersion = [[UIDevice currentDevice] systemVersion];
    NSArray *versions = [currentVersion componentsSeparatedByString:@"."];
    NSInteger majorVersion = [[versions firstObject] integerValue];
    return majorVersion <= 7;
}

+ (id)topViewController
{
    UIViewController *topViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (topViewController.presentedViewController) {
        topViewController = topViewController.presentedViewController;
    }
    return topViewController;
}

@end
