//
//  LocalNotificationManager.m
//  20141020_iOS8_NotificationAppSample
//
//  Created by aCetylAcid on 2014/10/20.
//  Copyright (c) 2014年 aCetylAcid. All rights reserved.
//

#import "LocalNotificationManager.h"

@implementation LocalNotificationManager

+ (void)scheduleLocalNotification:(NSArray*)times
{
    NSMutableArray *notifications = [[NSMutableArray alloc]init];
    for (NSNumber *t in times) {
        UILocalNotification *noti = [[UILocalNotification alloc] init];
        noti.fireDate = [[NSDate alloc] initWithTimeIntervalSinceNow:[t integerValue]];
        noti.timeZone = [NSTimeZone defaultTimeZone];
        noti.alertBody = [NSString stringWithFormat:@"%ld秒経ちました。", [t integerValue]];
        noti.soundName = UILocalNotificationDefaultSoundName;
        [notifications addObject:noti];
    }

    [[UIApplication sharedApplication] setScheduledLocalNotifications:notifications];
}

@end
