//
//  LocalNotificationManager.h
//  20141020_iOS8_NotificationAppSample
//
//  Created by aCetylAcid on 2014/10/20.
//  Copyright (c) 2014年 aCetylAcid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface LocalNotificationManager : NSObject

/**
 *  ローカル通知を指定時間後に送信する
 *
 *  @param times NSArray<NSNumber> : 何秒後に通知を表示するか
 */
+ (void)scheduleLocalNotification:(NSArray*)times;

@end
