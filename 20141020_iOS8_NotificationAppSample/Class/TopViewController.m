//
//  TopViewController.m
//  20141020_iOS8_NotificationAppSample
//
//  Created by aCetylAcid on 2014/10/20.
//  Copyright (c) 2014年 aCetylAcid. All rights reserved.
//

#import "TopViewController.h"

@interface TopViewController ()

@end

@implementation TopViewController

- (IBAction)localNotiBtnTapped:(id)sender {
    NSArray *times = @[[NSNumber numberWithInteger:5],
                      [NSNumber numberWithInteger:10],
                      [NSNumber numberWithInteger:15],
                      [NSNumber numberWithInteger:20],
                      [NSNumber numberWithInteger:25],
                       [NSNumber numberWithInteger:30]];
    [LocalNotificationManager scheduleLocalNotification:times];
}

@end
