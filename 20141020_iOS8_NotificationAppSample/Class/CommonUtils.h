//
//  CommonUtils.h
//  20141020_iOS8_NotificationAppSample
//
//  Created by aCetylAcid on 2014/10/21.
//  Copyright (c) 2014年 aCetylAcid. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CommonUtils : NSObject

/**
 *  OSのメジャーバージョンが7以前であるか
 *
 *  @return iOS7以前であればYES
 */
+ (BOOL)isIOS7orEarlier;


/**
 *  最前面のViewControllerを返す
 *
 *  @return 最前面のViewController
 */
+ (id)topViewController;

@end
