//
//  AppDelegate.h
//  20141020_iOS8_NotificationAppSample
//
//  Created by aCetylAcid on 2014/10/20.
//  Copyright (c) 2014年 aCetylAcid. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtils.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

